import { Console } from "console";
import { collapseTextChangeRangesAcrossMultipleVersions } from "typescript";

function cifraturaCesare(testo: string, key: number){
    let result: string = "";
    
    for (let char of testo){
        const encodedChar = char.charCodeAt(0) + key;
        result += String.fromCharCode(encodedChar);
    }
    return result;
}

function decrittazioneCesare(testo: string, key: number){
    return cifraturaCesare(testo, -key)
}

function betterCifraturaCesare(testo: string, key: number, shift: number=1){
    let result: string = "";
    
    for (let char of testo){
        const encodedChar = char.charCodeAt(0) + key;
        result += String.fromCharCode(encodedChar);
        key+=shift;
    }
    return result;
}

function decrittazioneBetterCesare(testo: string, key: number, shift: number=1){
    key = -key;
    let result: string = "";
    
    for (let char of testo){
        const encodedChar = char.charCodeAt(0) + key;
        result += String.fromCharCode(encodedChar);
        key-=shift;
    }
    return result;
}

function cifraturaVigenere(testo: string, key: string){
    let result: string = ""
    for (let i = 0; i < testo.length; i++){
        const encodedChar = result.charCodeAt(i) + key.charCodeAt(i%key.length) - "A".charCodeAt(0);
        result += String.fromCharCode(encodedChar);
    }
    return result;
}

function decrittazioneVigenere(testo: string, key: string){
    let result: String = ""
    for (let i = 0; i< testo.length; i++){
        const encodedChar = 
            result.charCodeAt(i) - (key.charCodeAt(i%key.length) - "A".charCodeAt(0));
        result += String.fromCharCode(encodedChar);
    }
    return result;
}

function cifraturaVernam(testo:string,key:string){
    let result = "";
    for (let i = 0; i<testo.length; i++){
        const encodedChar = testo.charCodeAt(i) ^ key.charCodeAt(1%key.length);
        result += String.fromCharCode(encodedChar);
    }
}

function cifraturaColonne(s: string, k: string){
    const sMatrix: string[][] = [];
    for(let char of k){
        sMatrix.push([]);
    }
    const cols = k.length;
    const rows = Math.ceil(s.length / cols);
    for(let y=0; y<rows; y++){
        for(let x=0; x<cols; x++){
            sMatrix[x].push(s.charAt(y * cols + x) || " ");
        }
    }
    const sortedKey = k.split("").sort().join("");
    const sortedMatrix: string[][] = [];
    for(let char of sortedKey){
        sortedMatrix.push(sMatrix[k.indexOf(char)]);
        k = k.replace(char, '\0')
    }
    let result = "";
    for(let x=0; x<cols; x++){
        for(let y=0; y<rows; y++){
            result += sortedMatrix[x][y]
        }
    }
}

function decreittazioneColonne(s: string, k: string){
    const sMatrix: string[][] = [];
    for(let char of k){
        sMatrix.push([]);
    }
    const cols = k.length;
    const rows = Math.ceil(s.length / cols);
    for(let x=0; x<cols; x++){
        for(let y=0; y<rows; y++){
            sMatrix[x].push(s.charAt(x * cols + y));
        }
    }
    console.log(sMatrix)
    let sortedKey = k.split("").sort().join("");
    const sortedMatrix: string[][] = [];
    for(let char of k){
        sortedMatrix.push(sMatrix[sortedKey.indexOf(char)])
        sortedKey = sortedKey.replace(char, '\0')
    }
    console.log(sortedMatrix);
    let result = "";
    for(let y=0; y<rows; y++){
        for(let x=0; x<cols; x++){
            result += sortedMatrix[x][y];
        }
    }
    return result.trimEnd();
}

const testo = "Berasi oggi non c'è"
const nKey = 5;
const key = "chiave"
const encryptedS = cifraturaColonne(testo, key)
console.log(encryptedS)
const decryptedS = decreittazioneColonne(testo, key)
console.log(decryptedS)