import { Component } from '@angular/core';

@Component({
  selector: 'app-my-component',
  templateUrl: './my-component.component.html',
  styleUrls: ['./my-component.component.scss']
})
export class MyComponentComponent {

  headerOne = "Testo demo";
  buttonOne = "Cliccami!";
  arrayOne = [
    "testo1",
    "testo2",
    "testo3",
    "testo4",
    "testo5",
    "testo6",
    "testo7",
    "testo8",
    "testo9",
    "testo10",
    "testo11"
  ]

  click(){
    console.log("Ciao mondo!")
  }

}
