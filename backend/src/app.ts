import express from 'express';
import { exec } from 'child_process';

const app = express();
const port = 1234;

app.get('/', (req, res) =>{
    res.send("Ho ricevuto una richiesta da: " + req.ip);
})

app.get('/files', (req, res) =>{
    // res.setHeader('content-type', 'application/json');
    res.type('json')
    exec('ls', (error, stdout, stderr) => {
        if(error){
            console.error(error);
            return;
        }
        const absolute = req.query.absolute

        if(absolute){
            exec("pwd", (error, stdout, stderr) =>{
                if(error){
                    console.error(error);
                    return true;
                }
                console.log(stdout);
            })
        }

        console.log(stdout);
        let files = stdout.split('\n');
        console.log(files)
        files = files.filter((file) =>{
            if (file != ''){
                return true 
            } else {
                return false
            }
        });
        console.log(files)
        res.status(200).send({
            code: 200,
            files: files,
        })
    })
})

app.listen(port, () => {
    console.log("[!] Server in ascolto sulla porta: " + port);
})