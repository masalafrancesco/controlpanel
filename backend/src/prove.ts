import * as CryptoJS from 'crypto-js';

function hashPassword(str: string){
    const hash= CryptoJS.SHA256(
        str
    ).toString();
    return hash;
}

console.log(hashPassword("Demo"));