"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const app = (0, express_1.default)();
const port = 1234;
app.get('/', (req, res) => {
    res.send("Ho ricevuto una richiesta da: " + req.ip);
});
app.listen(port, () => {
    console.log("[!] Server in ascolto sulla porta: " + port);
});
